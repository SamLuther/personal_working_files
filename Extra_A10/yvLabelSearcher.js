'use strict'

// function createYVLabelSearcher(csvFile, max, search){
//     var yvClient = require('bibletools').createYVClient();
//     const fs = require('fs')
//     var parse = require('csv-parse')
//     var count = 0
    
//     fs.readFile(csvFile, function (err, data) {

//         parse(data, {columns : true}, function(err, rows) {
//             if (err) {
//                 throw err;
//             }
//             rows.forEach(results => {
//                 if(search == results.label && count < max){
//                     console.log(results.usfm + "   " + results.cnt)
//                     //console.log(JSON.stringify(yvClient.promiseToGetReferencesAsText(111,results.usfm)));
//                     count++
//                     console.log(count)
//                 }else{
//                     return false
//                 }
//             })
//         })
//     })
// }
// var search = "fruit";
// var max = 11000;
// //createYVLabelSearcher(csvFile, max, search)


//
//{
//	"love": [
//		{
//			"usfm": "1CO.13.4",
//			"cnt": 6099
//		},
//		{
//			"usfm": "1CO.13.7",
//			"cnt": 6099
//		},
//		{
//			"usfm": "1CO.13.4",
//			"cnt": 6099
//		},
//
//	],
//	"faith": [
//	]
//}
//
//
//Use this Format ^^
//
const csvFile = process.argv[2];
function makeCsvToJson(csvFile){
    var Verses = require('./BibleNIV_format.json');
    var Typo = require('typo-js');
    var dictionary = new Typo('en_US');
    var fs = require('fs');
    var parse = require('csv-parse')
    //var yvClient = require('bibletools').createYVClient();
    var passageFormatter = require('bibletools').createFormatter();
    var array = []
    var uniqueArray = []
    var finalMap = {}
    var errorMap = {}
    var tenThousand = require('./10000englishWordsGood.json')
    var bibleWords = require('./bible_words.json')
    var count1 = 0;
    var count2 = 0;

    // var done = require('./yvlabelsMap1.json')
    // finalMap = done 

    function writeToBad(str,reason){
        count1++;
        if (!errorMap[reason]){
            errorMap[reason] = [str];
        }
        else{
            if(!errorMap[reason].includes(str)){
                errorMap[reason].push(str);
                count2++;
            }
        }
        if (count1%100000 === 0) {
            console.log("Writing Dropped Labels, " + count2 + " labels dropped");
            fs.writeFileSync('droppedLabels.json', JSON.stringify(errorMap, null, 4));
        }
        
    }

    function useSuggestions(suggestions,verse){
        var verseRay = verse.split(".");
        verseRay[0] = passageFormatter.singleRefToPassageObj(verse).book;
        function getVerseFromRef(verseRay){
            for(var i =0; i<Verses.length;i++){
                if(verseRay[0] === Verses[i].book && verseRay[1] === Verses[i].chapter  && verseRay[2] === Verses[i].verse){
                    return Verses[i].text;
                }
            }
        }
        var VerseText = getVerseFromRef(verseRay);
            for(var i=0;i<suggestions.length;i++){
                if(VerseText.includes(suggestions[i])){
                    return suggestions[i];
                }
            }
            return undefined;
    }

    function hasGoodCharacters(str){
        str = str.replace("'","");
        str = str.toLowerCase();

        var num = 0;        

        if(str.includes(" ")){
            var each = str.split(" ");
            for(var i=0; i<each.length;i++){
                if(!/^[a-z]+$/i.test(each[i])){
                    writeToBad(str,"Bad Characters");
                    //console.log(str + "  Bad Characters");
                    return false;
                }           
            }
            //console.log(str+"  Bad Chars");
            return true;
        }
        if(/^[a-z]+$/i.test(str)){
            //console.log(str)
            return true;
        }else{        
            var charArray = ["1","2","3","4","5","6","7","8","9","0"];
            for(var i = 0; i<charArray.length; i++){
                if(str.includes(charArray[i])){
                    return true;
                }
            }
            //console.log(str)
            writeToBad(str,"Bad Characters");
            return false;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //franc doesnt work very well so best not uncomment everything below here
    ////////////////////////////////////////////////////////////////////////////
    // function isEnglish(str){
    //     var langDetector = require('franc');
    //     var lang = langDetector.all(str);
    //     if(lang[0][0] === "eng" || lang[0][0] === "und"){
            
    //         return true;
    //     }
    //     //console.log(lang);
    //     writeToBad(str,"Not English");
    //     return false;
    // }
    
    function checkSpelling(str, verse){
        var charArray = ["1","2","3","4","5","6","7","8","9","0"];
        for(var i = 0; i<charArray.length; i++){
            if(str.includes(charArray[i])){
                return str;
            }
        }
        if(tenThousand.includes(str) || bibleWords.includes(str)){
            return str;
        }else{
            if(str.includes(" ")){
                var each = str.split(" ");
                var points =0;

                for(var i=0; i<each.length;i++){
                    if(bibleWords.includes(each[i])){
                        points++;
                    }
                    var is_spelled_correctly = dictionary.check(each[i]);     

                    if(is_spelled_correctly){

                        points++;
                    }
                    else{
                        writeToBad(str,"Misspelled");
                        // console.log(str+"  Misspelled");
                        return undefined;
                    }     
                }
                if(points >= each.length){
                    return str;
                }

            }
            var is_spelled_correctly = dictionary.check(str);
            if(is_spelled_correctly){

                return str;
            }
            else{

                var array_of_suggestions = dictionary.suggest(str);
                var checked = useSuggestions(array_of_suggestions,verse)

                if(checked === undefined){
                    writeToBad(str,"Misspelled");
                }
                return checked;
                //return undefined;
                
            }
        }
    }
    
    fs.readFile(csvFile, function (err, data){
        parse(data, {columns : true}, function(err, rows) {
            var count = 0;
            rows.forEach(results => {
                count++;
                if (count%100 === 0) //console.log(count+' of '+rows.length);
                if (count%100000 === 0) {
                    console.log('Writing Map, Map Has '+Object.keys(finalMap).length + " Labels");
                    fs.writeFileSync('yvlabelsMap.json', JSON.stringify(finalMap, null, 4));
                }
                if (!results.label) {
                    console.log('WARN: bad label '+JSON.stringify(results));
                    return false;
                }
                if(hasGoodCharacters(results.label)){
                    var checked = checkSpelling(results.label, results.usfm);
                    if(checked === undefined){
                        //console.log("Bad spelling")
                    }else{
                        if (!finalMap[checked]){//&& isEnglish(results.label)) {
                            finalMap[checked] = [];
                        }
                        try {
                            if(finalMap[checked].length < 51)
                                finalMap[checked].push({usfm:results.usfm, cnt:results.cnt});
                        } catch (e) {
                            console.log("WARNING - Failed on result: "+JSON.stringify(results, null, 4));
                        }  
                    }
                }
                else{
                }
            })
            console.log("Finl Write YAYYYYY!!!!")
            console.log("")
            console.log("                     __gggrgM**M#mggg__")
            console.log("                 __wgNN@/B*Pvvmpvv@d#v@N#Nw__")
            console.log("               _g#@0F_a*F#  _*F9m_ ,F9*__9NG#g_")
            console.log("            _mN#F  aMv    #pv    !q@    9NL v9#Qu_")
            console.log("           g#MF _pPvL  _g@v9L_  _gvv#__  gv9w_ 0N#p")
            console.log("         _0F jL*v   7_wF     #_gF     9gjF   vbJ  9h_")
            console.log("        j#  gAF    _@NL     _g@#_      J@u_    2#_  #_")
            console.log("       ,FF_#v 9_ _#v  vb_  g@   vhg  _#v  !q_ jF v*_09_")
            console.log("       F Nv    #pv      Ng@       `#gv      vw@    v# t")
            console.log("      j p#    gv9_     g@v9_      gPv#_     gFvq    Pb L")
            console.log("      0J  k _@   9g_ j#v   vb_  j#v   vb_ _dv   q_ g  ##")
            console.log("      #F  `NF     v#gv       vMdv       5N#      9Wv  j#")
            console.log("      #k  jFb_    g@vq_     _*v9m_     _*vR_    _#Np  J#")
            console.log("      tApjF  9g  Jv   9M_ _mv    9%_ _*v   v#  gF  9_jNF")
            console.log("       k`N    vq#       9g@        #gF       ##v    #vj")
            console.log("       `_0q_   #vq_    _&v9p_    _gv`L_    _*v#   jAF,'")
            console.log("        9# vb_j   vb_ gv    *g _gF    9_ g#v  vL_*vqNF")
            console.log("         vb_ v#_    vNL      _B#      _I@     j#v _#v")
            console.log("           NM_0v*g_ jvv9u_  gP  q_  _w@ ]_ _g*vF_g@")
            console.log("            vNNh_ !w#_   9#gv    vm*v   _#*v _dN@v")
            console.log("               9##g_0@q__ #v4_  j*vk __*NF_g#@Pv")
            console.log("                 v9NN#gIPNL_ vb@v _2MvLg#N@Fv")
            console.log("                     vvP@*NN#gEZgNN@#@Pvv")

            fs.writeFileSync('droppedLabels.json', JSON.stringify(errorMap, null, 4));
            fs.writeFileSync('yvlabelsMap.json', JSON.stringify(finalMap, null, 4));
            console.log("Total Dropped Labels: " + count2)
            console.log("Total Labels: " + Object.keys(finalMap).length)
            //console.log(finalMap)
            // uniqueArray = array.filter(function(item, pos) {
            //     return array.indexOf(item) == pos;
            // })
            // console.log(uniqueArray)
        })
    })
}
makeCsvToJson(csvFile)